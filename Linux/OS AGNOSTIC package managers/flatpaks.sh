flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#flatpak remote-add --user --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
flatpak update --appstream

#utillity
flatpak install --user flathub    in.srev.guiscrcpy  io.github.JaGoLi.ytdl_gui   -y

#essential
flatpak install --user flathub io.github.prateekmedia.appimagepool 
flatpak install  org.gnome.Platform.Compat.i386/x86_64/41 org.gnome.Platform.Compat.i386/x86_64/42 org.freedesktop.Platform.GL32.default/x86_64/21.08 -y

#browsers
flatpak install flathub app/org.mozilla.firefox/x86_64/stable  -y

#work essentials
flatpak install flathub com.anydesk.Anydesk  md.obsidian.Obsidian org.libreoffice.LibreOffice/x86_64/stable  -y

####GAMES####
#The Linux Game Launcher
flatpak install --user flathub net.lutris.Lutris-y
#Game Launchers
flatpak install --user com.valvesoftware.Steam com.gitlab.librebob.Athenaeum -y
#flatpak install flathub io.github.sharkwouter.Minigalaxy com.valvesoftware.Steam  com.heroicgameslauncher.hgl com.gitlab.librebob.Athenaeum io.itch.itch   -y
#flatpak install flathub  dev.goats.xivlauncher  com.mojang.Minecraft -y
#Console Emulators
#flatpak install flathub org.yuzu_emu.yuzu  org.desmume.DeSmuME  app.xemu.xemu net.rpcs3.RPCS3 io.github.m64p.m64p com.dosbox.DOSBox org.duckstation.DuckStation org.flycast.Flycast org.ppsspp.PPSSPP org.tuxfamily.hatari org.citra_emu.citra org.purei.Play io.github.dosbox-staging io.mgba.mGBA ca._0ldsk00l.Nestopia com.carpeludum.KegaFusion org.DolphinEmu.dolphin-emu -y 

#miniGames
#flatpak install flathub net.supertuxkart.SuperTuxKart -y

#Streaming game platforms
#flatpak install flathub re.chiaki.Chiaki  io.github.hmlendea.geforcenow-electron -y

#Proton Managers
flatpak install flathub  com.github.Matoking.protontricks net.davidotek.pupgui2 -y
#Gamming Utillities
flatpak install flathub org.freedesktop.Platform.VulkanLayer.MangoHud com.valvesoftware.Steam.Utility.steamtinkerlaunch  com.valvesoftware.Steam.Utility.gamescope com.valvesoftware.Steam.CompatibilityTool.Boxtron -y

#Hardware Utillities
flatpak install flathub org.openrgb.OpenRGB -y

###Dependancies wine
flatpak install \
com.valvesoftware.Steam.Utility.steamtinkerlaunch \
com.valvesoftware.Steam.CompatibilityTool.Proton-GE  \
com.valvesoftware.Steam.CompatibilityTool.Proton -y
###Dependancies wine


####GAMES####

#Wine
flatpak install  flathub   org.winehq.Wine org.winehq.Wine.mono org.winehq.Wine.gecko org.winehq.Wine.DLLs.dxvk  com.usebottles.bottles  -y

#torrents
flatpak install  flathub de.haeckerfelix.Fragments -y

###COMMUNICATION APPS###
#comms
flatpak install  flathub  thunderbird  us.zoom.Zoom  com.skype.Client org.gnome.Fractal  -y
#Chats
flatpak install flathub  com.chatterino.chatterino com.discordapp.Discord de.shorsh.discord-screenaudio   -y
#Social Media Messengers
flatpak install flathub org.signal.Signal com.github.eneshecan.WhatsAppForLinux  com.viber.Viber com.sindresorhus.Caprine  uk.co.ibboard.cawbird -y

#Controller mapper
flatpak install flathub io.github.antimicrox.antimicrox -y

#Translator Programs
flatpak install flathub net.poedit.Poedit -y

###COMMUNICATION APPS###

###engineering###
##Cad 
flatpak install flathub  org.freecadweb.FreeCAD org.kicad.KiCad com.diy_fever.DIYLayoutCreator io.github.dubstar_04.design  -y
##Networking 
flatpak install flathub de.lernsoftware_filius.Filius    -y
##Complex Calculation and maths
flatpak install flathub  org.octave.Octave -y
##Robotics
flatpak install flathub cc.arduino.IDE2 -y
#Vnc Connections
flatpak install flathub org.gnome.Connections -y
###engineering###

###development###
#Languages
flatpak install flathub   org.freedesktop.Sdk.Extension.vala org.freedesktop.Sdk.Extension.mono6   -y
#IDES
flatpak install flathub --user  com.jetbrains.IntelliJ-IDEA-Community  -y
#flatpak install flathub   org.gnome.Builder com.visualstudio.code    com.jetbrains.Rider com.jetbrains.IntelliJ-IDEA-Community -y
##Git Managers
flatpak install flathub  org.gnome.gitg -y
##Game Engines
flatpak install flathub   org.godotengine.Godot -y
#Docker
flatpak install flathub   io.podman_desktop.PodmanDesktop -y
###development###

###Multimedia###
#multimedia players local files
flatpak install  flathub   io.mpv.Mpv/x86_64/stable -y
#find song file info
flatpak install flathub org.musicbrainz.Picard -y
#multimedia players services
flatpak install flathub com.github.KRTirtho.Spotube   com.spotify.Client org.js.nuclear.Nuclear  sh.cider.Cider  -y
#Multimedia production 2d drawing,editing
flatpak install  flathub org.inkscape.Inkscape com.github.libresprite.LibreSprite org.gimp.GIMP org.kde.krita org.darktable.Darktable  -y
#Multimedia production 3d
flatpak install flathub  org.blender.Blender -y 
#multimedia production video
flatpak install  flathub    com.obsproject.Studio   org.kde.kdenlive   -y
#Multimedia production audio
flatpak install flathub org.ardour.Ardour -y
#Multimedia production interactive stories
flatpak install flathub org.twinery.Twine -y
###Multimedia###
#reading
flatpak install flathub com.github.johnfactotum.Foliate -y

###Entertainment###
#flatpak install com.stremio.Stremio -y

###Wallpaper Engine like lnx flatpaked
flatpak install flathub io.github.jeffshee.Hidamari -y

###PRIVACY###
##Metadata
flatpak install fr.romainvigier.MetadataCleaner -y
