flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak update --appstream

####Gnome###
##gnome Apps
##essential
flatpak install org.gnome.DejaDup org.gnome.Firmware org.gnome.Todo org.gnome.Boxes org.gnome.Recipes org.gnome.design.IconLibrary -y   

##extra
flatpak install  flathub --user  org.gnome.Weather org.gnome.TextEditor  org.gnome.seahorse.Application org.gnome.Calendar  org.gnome.Photos  org.gnome.Evince  org.gnome.FileRoller org.gnome.Totem org.gnome.baobab   org.gnome.gThumb  org.gnome.Calculator org.gnome.NautilusPreviewer   org.gnome.Notes org.gnome.Contacts  org.gnome.SoundRecorder org.gnome.Contacts org.gnome.eog   org.gnome.Extensions org.gnome.Calculator  org.gnome.clocks  -y  
 ##can be ignored
flatpak install  flathub --user  org.gnome.ColorViewer   org.gnome.SoundRecorder      org.gnome.FontManager org.gnome.Mahjongg  org.gnome.Cheese org.gnome.Characters     org.gnome.eog     -y
 ##gnome games
 flatpak install flathub   org.gnome.Aisleriot org.gnome.Sudoku   org.gnome.Mines -y
#gnome like apps
#flatpak install  flathub --user  com.github.liferooter.textpieces com.rafaelmardojai.Blanket com.github.hugolabe.Wike  -y
###Gnome
#Customization
flatpak install flathub --user com.github.GradienceTeam.Gradience com.mattjakeman.ExtensionManager   com.github.tchx84.Flatseal io.github.realmazharhussain.GdmSettings -y
#Extra
flatpak install flathub io.github.Foldex.AdwSteamGtk -y
flatpak install org.gnome.Gtranslator  org.pitivi.Pitivi org.gnome.Podcasts  -y

